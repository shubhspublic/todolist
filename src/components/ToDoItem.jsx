
import React from "react";
function ToDoItem(props) {
  return (
    <div>
      <li onClick={() => {
         props.onChecked(props.id)
      }}>
        {props.text}
      </li>
    </div>
  );
}

/* condition to uncheck on click

import React, { useState } from "react";
function ToDoItem(props) {
  const [isDone, setIsDone] = useState(false);
  function handleClick() {
    setIsDone((prevValue) => {
      return !prevValue;
    });
  }

  return (
    <div onClick={handleClick}>
      <li style={{ textDecoration: isDone ? "line-through" : "none" }}>
        {props.text}
      </li>
    </div>
  );
}

*/

export default ToDoItem;
